/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swingtest;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
public class ComboBoxTest extends JFrame{
    
    private JComboBox imagesJComboBox; // combobox to hold names of icons
    private JLabel label; // label to display selected icon
    private static final String[] names =
    { "bug1.gif", "bug2.gif", "travelbug.gif", "buganim.gif" };
    private Icon[] icons = {
    new ImageIcon( getClass().getResource( names[ 0 ] ) ),
    new ImageIcon( getClass().getResource( names[ 1 ] ) ),
    new ImageIcon( getClass().getResource( names[ 2 ] ) ),
    new ImageIcon( getClass().getResource( names[ 3 ] ) ) };
    // ComboBoxFrame constructor adds JComboBox to JFrame
    public ComboBoxTest()
    {
        super("Testing JComboBox");
        setLayout(new FlowLayout());
        imagesJComboBox = new JComboBox( names ); // set up JComboBox
        imagesJComboBox.setMaximumRowCount( 3 ); // display three rows
        imagesJComboBox.addItemListener(
            new ItemListener() // anonymous inner class
            {
                public void itemStateChanged( ItemEvent event )
                {
                    if ( event.getStateChange() == ItemEvent.SELECTED )
                    label.setIcon( icons[
                    imagesJComboBox.getSelectedIndex() ] );
                }
            }
        
        );
        add( imagesJComboBox ); // add combobox to JFrame
        label = new JLabel( icons[ 0 ] ); // display first icon
        add( label ); 
        
        setSize(200,200);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setVisible(true);
    }
    
    public static void main(String args[]){
        new ComboBoxTest();
    }
    
}
