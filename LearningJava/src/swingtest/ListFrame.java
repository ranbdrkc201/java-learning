/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swingtest;

import java.awt.FlowLayout;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.ListSelectionModel;
public class ListFrame extends JFrame
{
    private JList colorJList; // list to display colors
    private static final String[] colorNames = { "Black", "Blue", "Cyan",
    "Dark Gray", "Gray", "Green", "Light Gray", "Magenta",
    "Orange", "Pink", "Red", "White", "Yellow" };
    private static final Color[] colors = { Color.BLACK, Color.BLUE,
    Color.CYAN, Color.DARK_GRAY, Color.GRAY, Color.GREEN,
    Color.LIGHT_GRAY, Color.MAGENTA, Color.ORANGE, Color.PINK,
    Color.RED, Color.WHITE, Color.YELLOW };
    // ListFrame constructor add JScrollPane containing JList to JFrame
    public ListFrame()
    {
            super( "List Test" );
            setLayout( new FlowLayout() ); // set frame layout
            colorJList = new JList( colorNames ); // create with colorNames
            colorJList.setVisibleRowCount( 13 ); // display five rows at once
            // do not allow multiple selections
            colorJList.setSelectionMode( ListSelectionModel.SINGLE_SELECTION );
            // add a JScrollPane containing JList to frame
            add( new JScrollPane( colorJList ) );
            colorJList.addListSelectionListener(
            new ListSelectionListener() // anonymous inner class
            {
            // handle list selection events
            public void valueChanged( ListSelectionEvent event )
            {
            getContentPane().setBackground(
            colors[colorJList.getSelectedIndex() ] );
            } // end method valueChanged
            } // end anonymous inner class
            ); // end call to addListSelectionListener
            
            setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
            setSize( 650, 550 ); // set frame size
            setVisible( true ); // display frame

    } // end ListFrame constructor
    public static void main(String args[]){
        new ListFrame();
    }
} // end class ListFrame
