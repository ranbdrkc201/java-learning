/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swingtest;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class CheckBoxTest extends JFrame {
    
    private JTextField txtField;
    private JCheckBox boldText;
    private JCheckBox italicText;
    CheckBoxTest(){
        super("Check Box Test");
        setSize(300,300);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLayout(new FlowLayout());
        
        txtField=new JTextField("Hello I am Ran Bahadur kc",20);        
        txtField.setFont(new Font("Sarif",Font.PLAIN,15));
        add(txtField);
        
        boldText=new JCheckBox("Bold");
        italicText=new JCheckBox("Italic");
        
        add(boldText);
        add(italicText);
        
        CheckBoxHandler handler=new CheckBoxHandler();
        boldText.addItemListener(handler);
        italicText.addItemListener(handler);
        setVisible(true);
    }
        
        private class CheckBoxHandler implements ItemListener{
            public void itemStateChanged(ItemEvent e){
                
                
               Font font=null;
              if(boldText.isSelected() && italicText.isSelected()){
                  font=new Font("Sarif",Font.BOLD+Font.ITALIC,14);
              } 
              else if(boldText.isSelected()){
                  font=new Font("Sarif",Font.BOLD,14);
              }else if(italicText.isSelected()){
                 font=new Font("Sarif",Font.ITALIC,14);
              }else{
                  font=new Font("Sarif",Font.PLAIN,14);
              }
              txtField.setFont(font);
           }

        }
       
    
    public static void main(String args[]){
        new CheckBoxTest();
    }
}
