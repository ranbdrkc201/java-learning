package Training2075;
class Rn{
	private int hour;
	private int minute;
	private int second;
	Rn(){
		hour=20;
		minute=30;
		second=40;
				
	}
	
	public void setHour(int h) {
		hour=((h>=0 && h<24) ? h:0);
	}
	public void setMinute(int m) {
		minute=((m>=0 && m<60) ? m:0);
		}
	public void setSecond(int s) {
		second=((s>=0 && s<60) ? s:0);
	}
	
	public int getHour() {
		return hour;
	}
	public int getMinute() {
		return minute;
	}
	public int getSecond() {
		return second;
	}
	public String toMilitary() {
		System.out.println("---------------------------------");
		return String.format("%02d:%02d:%02d", getHour(),getMinute(),getSecond());
	}
}

public class GetSetMethod {
	public static void main(String args[]) {
	Rn obj=new Rn();
	System.out.println(obj.toMilitary());
	}

}
