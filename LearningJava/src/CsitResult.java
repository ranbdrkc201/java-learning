
import java.util.Scanner;

public class CsitResult {

    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        float daa, ai, cn, eco, sim, total;
        int per;
        System.out.println("Enter the number of all subject:");
        daa = scan.nextFloat();
        ai = scan.nextFloat();
        cn = scan.nextFloat();
        eco = scan.nextFloat();
        sim = scan.nextFloat();
        total = daa + ai + cn + eco + sim;
        per = (int) (((total) / 500) * 100);
        System.out.println("Your Total number =" + total);

        if(per>=80) {
        	System.out.println("Distinction");
        }else if(per>=70 && per<80) {
        	System.out.println("First Division");
        }else if(per>=60 && per<70) {
        	System.out.println("Second Division");
        }else if(per>=50 && per<60) {
        	System.out.println("Third Division");
        }else {
        	System.out.println("Failed");
        }

    }
}
