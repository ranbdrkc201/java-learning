
package EventHandling;

import java.awt.*;

public class EventTest extends Frame{
    
    TextField t;
    Button b;
    
    EventTest(){
        setTitle("Event Handling Test Class");
       
        setLayout(new FlowLayout(FlowLayout.CENTER,20,25));
        setSize(300,200);
        
        t=new TextField(20);
        b=new Button("Click");
        add(t);
        add(b);
        
        setVisible(true);
    }
    
    public boolean action(Event e,Object obj){
        String caption=(String)obj;
        String msg="Hello RN Khatri";
        if(e.target instanceof Button){
            if(caption=="Click"){
                t.setText(msg);
            }  
        }
        return true;
    }
    public static void main(String args[]){
       new EventTest();
    }
}
