/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EventHandling;

import java.awt.GridLayout;
import java.awt.event.*;
import javax.swing.*;
public class GetActionCommand extends JFrame implements ActionListener,ItemListener{
 
    JLabel lb;
    JButton bt[];
    JRadioButton rb1,rb2;
    JComboBox cb;
    GetActionCommand(){
       lb=new JLabel();
       
      // lb.setBounds(60,70,170,20);
      bt=new JButton[10];
       setDefaultCloseOperation(DISPOSE_ON_CLOSE);
       setLayout(new GridLayout(4,3));
       for(int i=0;i<10;i++){
           bt[i]=new JButton(String.valueOf(i));
           add(bt[i]);
           bt[i].addActionListener(this);
       }
       
       setSize(300,200);
       
       rb1=new JRadioButton("Male");
       rb2=new JRadioButton("Female");
       ButtonGroup bg=new ButtonGroup();
       bg.add(rb1);
       bg.add(rb2);
       add(rb1);
       add(rb2);
       
       rb1.addItemListener(this);
        rb2.addItemListener(this);
        
       
        String[] str={"Nepal","Chaina","India"};
        cb=new JComboBox(str);
        add(cb);
        cb.addItemListener(this);
       
       
       setVisible(true);
    }
    
    
    public void actionPerformed(ActionEvent e){
        JOptionPane.showMessageDialog(rootPane, e.getActionCommand());
    }
    
    public void itemStateChanged(ItemEvent ie){
        if(ie.getSource()==rb1 && rb1.isSelected()==true){
            JOptionPane.showMessageDialog(rootPane, "Male");
        
        }
        if(ie.getSource()==rb2 && rb2.isSelected()==true){
            JOptionPane.showMessageDialog(rootPane, "Female");
        }
        
        if(ie.getSource()==cb && ie.getStateChange()==ie.SELECTED){
            JOptionPane.showMessageDialog(rootPane, cb.getSelectedItem());
        }
    }
    public static void main(String args[]){
        new GetActionCommand();
    }
}
