package Thread;
class X extends Thread{
	public void run()
	{
		for(int i=0; i<5; i++) {
			if(i==3) 
				yield();
			System.out.println("From thread X:i=" +i);
		}
		System.out.println("Exit From X");
	}
}
class Y extends Thread{
	public void run() {
		for(int j=0; j<5; j++) {
			if(j==3)
				stop();
			System.out.println("From thread Y: j="+j);
		}
		System.out.println("Exit From Y");
	}
}
class Z extends Thread{
	public void run() {
		for(int k=0; k<=5; k++) {
			if(k==3) {
				try {
					sleep(1000);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			System.out.println("From Thread Z :K="+k);
		}
		System.out.println("Exit from C");
	}
	
}
public class SleepMethodUse {
	public static void main(String args[]) {
		X a=new X();
		Y b= new Y();
		Z c=new Z();
		a.start();
		b.start();
		c.start();
	}
}
