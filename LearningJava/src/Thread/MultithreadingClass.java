package Thread;
class A extends Thread{
	public void run()
	{
		for(int i=0; i<5; i++) {
			System.out.println("From thread A:i=" +i);
		}
		System.out.println("Exit From A");
	}
}
class B extends Thread{
	public void run() {
		for(int j=0; j<5; j++) {
			System.out.println("From thread B: j="+j);
		}
		System.out.println("Exit From B");
	}
}

public class MultithreadingClass {
public static void main(String args[]) {
	A a=new A();
	B b= new B();
	a.start();
	b.start();
}
}
