package Thread;
class C implements Runnable{
	public void run()
	{
		for(int i=0; i<5; i++) {
			System.out.println("From thread A:i=" +i);
		}
		System.out.println("Exit From A");
	}
}
class D implements Runnable{
	public void run() {
		for(int j=0; j<5; j++) {
			System.out.println("From thread B: j="+j);
		}
		System.out.println("Exit From B");
	}
}
public class RunnbleInterface {
	public static void main(String args[]) {
		C a=new C();
		D b= new D();
		Thread t1=new Thread(a);
		Thread t2= new Thread(b);
		t1.start();
		t2.start();
	}
}
