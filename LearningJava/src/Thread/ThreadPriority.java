package Thread;
class X1 extends Thread{
	public void run()
	{
		for(int i=0; i<5; i++) {
			if(i==3) 
				yield();
			System.out.println("From thread X:i=" +i);
		}
		System.out.println("Exit From X");
	}
}
class Y1 extends Thread{
	public void run() {
		for(int j=0; j<5; j++) {
			if(j==3)
				stop();
			System.out.println("From thread Y: j="+j);
		}
		System.out.println("Exit From Y");
	}
}
class Z1 extends Thread{
	public void run() {
		for(int k=0; k<=5; k++) {
			if(k==3) {
				try {
					sleep(1000);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			System.out.println("From Thread Z :K="+k);
		}
		System.out.println("Exit from C");
	}
	
}

public class ThreadPriority {
	public static void main(String args[]) {
		X1 a=new X1();
		Y1 b= new Y1();
		Z1 c=new Z1();
		a.setPriority(Thread.MIN_PRIORITY);
		b.setPriority(a.getPriority()+1);
		c.setPriority(10);
		System.out.println("Start X1");
		a.start();
		System.out.println("Start Y1");
		b.start();
		System.out.println("Start Z1");
		c.start();
	}
}
