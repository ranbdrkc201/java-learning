/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package URL;

import java.net.*;

public class ParseURL {
    public static void main(String args[])throws MalformedURLException{
        URL url=new URL("https://www.youtube.com/watch?v=asYxxtiWUyw");
        System.out.println("Protocol = "+url.getProtocol());
        System.out.println("Authority = "+url.getAuthority()); 
        System.out.println("Host = "+url.getHost());
        System.out.println("Port = "+url.getPort());
        System.out.println("query = "+url.getQuery());
        System.out.println("Filename = "+url.getFile());
        System.out.println("Ref = "+url.getRef());
        System.out.println("User Info = "+url.getUserInfo());
    }
}
