/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RMI;
import java.rmi.*;
import java.rmi.registry.*;
import java.rmi.server.*;

public class RMIServer extends UnicastRemoteObject implements RMI{    
    public RMIServer() throws RemoteException{
        super();
    }
    public int sum(int a, int b) throws RemoteException {
        return (a+b);
    }
    public int sub(int a, int b) throws RemoteException {
        return (a-b);
    }
    
    public static void main(String args[]){
        try {
            Registry reg=LocateRegistry.createRegistry(1011);
            reg.rebind("server", new RMIServer());
            System.out.println("Server Started");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
}
