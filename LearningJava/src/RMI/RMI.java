/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RMI;
import java.rmi.*;
public interface RMI extends Remote{
    public int sum(int a, int b) throws RemoteException;
    public int sub(int a, int b) throws RemoteException;
}
