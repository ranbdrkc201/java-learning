/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RMI;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
public class RMIClient {
    public static void main(String args[]){
        try {
            Registry reg=LocateRegistry.getRegistry("localhost",1011);
            RMI rmi=(RMI)reg.lookup("server");
            System.out.println("Server Connected");
            System.out.println("Sum of Two number="+rmi.sum(100, 250));
             System.out.println("Subtract of Two number="+rmi.sub(150, 100));
            
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
