class one
{
	void callMe() {
		System.out.println("This is class A");
	}
}
class Two extends one{
	void callMe() {
		System.out.println("This is class B");
	}
}
class Three extends Two{
	void callMe()
	{
		System.out.println("This is class C");
	}
}

public class DynamicMethodDisplay {
public static void main(String a[]) {
	one on= new one();
	Two t=new Two();
	Three th=new Three();
	one r;
	r=th;
	r.callMe();
	r=t;
	r.callMe();
	r=on;
	on.callMe();
}
}
