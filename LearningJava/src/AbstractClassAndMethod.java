/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DELL
 */
abstract class shape{
    abstract void draw();
}
class Circles extends shape{
    void draw(){
        System.out.println("Drawing Circle");
    }
}
 class Rectangles extends shape{
     void draw(){
         System.out.println("Drawing Rectangle");
     }
 }
public class AbstractClassAndMethod {
    public static void main(String a[]){
        Circles c=new Circles();
        c.draw();
    }
}
