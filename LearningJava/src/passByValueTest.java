class PassByValue
{
	void doSomething(int i, int j)
	{
		i=i/2;
		j=j/2;
		
	}
	
}


public class passByValueTest {
	public static void main(String ar[])
	{
		PassByValue p=new PassByValue();
		int a=100, b=200;
		System.out.println("The value of a  and b after call:"+a+" "+b);
		p.doSomething(a,b);
		System.out.println("The value of a  and b after call:"+a+" "+b);
	}
}

