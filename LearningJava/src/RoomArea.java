class Room
{
	int length,width;
	Room(int l)
	{
		length=width=l;
	}
	Room(int l, int w)
	{
		length=l;
		width=w;
	}
	int roomArea()
	{
		return (length*width);
	}
}
public class RoomArea {
	public static void main(String a[])
	{
		Room r1,r2;
		r1=new Room(5);
		r2=new Room(5,3);
		int area1=r1.roomArea();
		int area2=r2.roomArea();
		System.out.println("Area of Room 1="+area1);
		System.out.println("Area of Room 2="+area2);
	}
}
