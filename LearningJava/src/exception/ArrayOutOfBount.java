package exception;

public class ArrayOutOfBount {
 public static void main(String ar[]) {
	 try {
		int a[]=new int[5];
		a[4]=30/6;
	} catch (ArithmeticException e) {
		// TODO: handle exception
		System.out.println("divide by zero");
	}
	 catch(ArrayIndexOutOfBoundsException e) {
			// TODO: handle exception
		 System.out.println("Invalid Index");
	}
	 catch (Exception e) {
			// TODO: handle exception
		 System.out.println("error="+e.getMessage());
	}
 }
}
