package exception;
class InvalidAgeException extends Exception{
	public InvalidAgeException( String message) {
		// TODO Auto-generated constructor stub
		super(message);
	}
}
public class ThrowsKeywordDemo {
static void validate(int age) throws InvalidAgeException, ArithmeticException{
	if(age>18) {
		System.out.println("Wlecome to vote");
	}else {
		throw new InvalidAgeException("Invalid Age");
		
	}
}
public static void main(String arge[]) {
	try {
		validate(15);
		
	} catch (InvalidAgeException | ArithmeticException e) {
		// TODO: handle exception
		System.out.println(e.getMessage());
	}
}
}
