class PassByReference
{
	int a,b;
	PassByReference(int i, int j)
	{
		a=j;b=i;
		System.out.println("First Call the Constructor:"+a+""+b);
	}
	void doSomething(PassByReference obj)
	{
		obj.a=obj.a/2;
		obj.b=obj.b/2;
	}
}



public class PassByReferenceTest {
	public static void main(String a[])
	{
		PassByReference p=new PassByReference(200, 400);
		System.out.println("The value of a and b before call:"+p.a+" "+p.b);
		p.doSomething(p);
		System.out.println("The value of a and b after call:"+p.a+" "+p.b);
	}
}
