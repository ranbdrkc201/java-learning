
package SendMail;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

public class JavaMailAPIs {
    public static void send(String to, String sub,String msg, final String from, final String pass) 
    {
        Properties props = new Properties();

        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");	
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        
        Session session = Session.getDefaultInstance(props,new Authenticator() 
        {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() 
            {
                return new PasswordAuthentication(from, pass);
            }
        });

        try 
        {
            Message message = new MimeMessage(session);
            
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject(sub);
            message.setText(msg);

            Transport.send(message);
            
        } catch (MessagingException e) {
            
            throw new RuntimeException(e);
        }
        
    }
    public static void main(String args[]){
        send("ranbdrkc201@gmail.com", "JavaMailAPIs","I am Ran Bahadur kc", "learnudemy649@gmail.com", "@learnudemy5596");
    }
}
