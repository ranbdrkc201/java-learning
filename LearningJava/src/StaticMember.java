

public class StaticMember {
	static int a=5;
	
	static int b;
	static void printValues(int x)//static method
	{
		System.out.println("X="+x);
		System.out.println("a="+a);
		System.out.println("b="+b);
	}
	static
	{
		System.out.println("Static block initialized");
		b=a*5;
	}
	public static void main(String a[])
	{
		System.out.println("First");
		printValues(100);
	}
}
