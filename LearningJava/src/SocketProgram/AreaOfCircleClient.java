/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SocketProgram;

import java.net.*;
import java.io.*;

public class AreaOfCircleClient {
      
    public static void main(String args[]) throws Exception{
        Socket s=new Socket("localhost",2563);
        
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        String rad;
        System.out.println("Enter the Radious of Circle");
        rad=br.readLine();
        PrintStream ps=new PrintStream(s.getOutputStream());
        ps.println(rad);
        
        BufferedReader fs=new BufferedReader(new InputStreamReader(s.getInputStream()));
        String result=fs.readLine();
        
        System.out.println("Area of circle From Server :"+result);
        br.close();
        ps.close();
        fs.close();
        s.close();
        
    }
}
