
package SocketProgram;
import java.net.*;
import java.io.*;

public class SimpleChatClient {
    public static void main(String args[]) throws Exception{
        Socket s=new Socket("localhost",2222);
        BufferedReader br=new BufferedReader(new InputStreamReader(s.getInputStream()));
        BufferedReader bw=new BufferedReader(new InputStreamReader(System.in));
        PrintStream ps=new PrintStream(s.getOutputStream());
        
        String rmsg="",wmsg="";
        do{
            rmsg=br.readLine();
            System.out.println(rmsg);
            
            System.out.println("Client: ");
            wmsg=br.readLine();
            ps.println(wmsg);
            
        }while(rmsg.equals("bye")!=true);
        
        br.close();
        bw.close();
        ps.close();
        s.close();
    }
}
