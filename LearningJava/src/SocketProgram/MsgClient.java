/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SocketProgram;

//clientv
import java.net.*;
import java.io.*;
import java.util.*;


public class MsgClient{

	public static void main(String[] args) throws IOException {
		Socket cs=new Socket("localhost",6667);
                
		Scanner ins=new Scanner(cs.getInputStream());
		PrintWriter outs=new PrintWriter(cs.getOutputStream(),true);
		outs.println("Hello Server");
		String s=ins.nextLine();
		System.out.println("From Server:"+s);

		ins.close();
		outs.close();
		cs.close();
	}
}
