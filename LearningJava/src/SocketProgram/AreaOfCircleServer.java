/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SocketProgram;

import java.net.*;
import java.io.*;
public class AreaOfCircleServer {
    public static void main(String args[]) throws Exception{
        ServerSocket ss=new ServerSocket(2563);
        System.out.println("Waiting");
        Socket s=ss.accept();
        System.out.println("Connection Create:");
        BufferedReader br=new BufferedReader(new InputStreamReader(s.getInputStream()));
        double rad,area;
        String result;
        
        rad=Double.parseDouble(br.readLine());
        System.out.println("Radius From Client:"+rad);
        
        area=3.14*rad*rad;
        PrintStream ps= new PrintStream(s.getOutputStream());
        ps.println(area);
        
        br.close();
        s.close();
        ss.close();
        ps.close();
        
        
    }
}
