/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SocketProgram;

import java.net.*;
import java.io.*;
import java.util.*;


public class MsgServer{

	public static void main(String[] args) throws IOException {
		ServerSocket ss=new ServerSocket(6667);

		Socket cs=ss.accept();

		Scanner ins=new Scanner(cs.getInputStream());
		PrintWriter outs=new PrintWriter(cs.getOutputStream(),true);

		String s=ins.nextLine();
		System.out.println("From Client:"+s);

		ins.close();
		outs.close();
		cs.close();
		ss.close();
	}
}