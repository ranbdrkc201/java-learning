
public class NestingTest {
	public static void main(String a[])
	{
		NestingObj obj=new NestingObj(5,6);
		obj.display();
	}
	
}

class NestingObj
{
	int m,n;
	NestingObj(int x, int y){
		m=x; n=y;
	}
	int largest()
	{
		if(m>n)
			return(m);
		else
			return (n);
	}
	void display() {
		int large=largest();//nesting method
		System.out.println("Largest="+large);
	}
}
