
public class QuicSortAlgorithm {
	public static void main(String a[]){
		int[] arr1 = {57, 85, 27,33,44, 17, 68};
		int[] arr2 = doInsertionSort(arr1);
		for(int i:arr2){
			System.out.print(i);
			System.out.print(", ");
		}
	}
	
	public static int[] doInsertionSort(int[] input){
		
	    int temp;
	    for (int i = 1; i < input.length; i++) {
		    for(int j = i ; j > 0 ; j--){
			    if(input[j] < input[j-1]){
				    temp = input[j];
				    input[j] = input[j-1];
				    input[j-1] = temp;
			    }
		    }
	    }
	    return input;
	}
}
