package Inheritance;
class Room
{
	int length, breadth;
	int area()
	{
		return(length*breadth);
	}
}

class BedRoom extends Room {
	int height;
	void setData(int l, int b, int h)
	{
		length=l;
		breadth=b;
		height=h;
	}
	int volume()
	{
		return(length*breadth*height);
	}
	
}
public class SingleInheritance {
	public static void main(String a[]) {
		BedRoom room=new BedRoom();
		room.setData(25,20, 15);
		int area=room.area();
		int volume=room.volume();
		System.out.println("Area="+area);
		System.out.println("Volume="+volume);
	}
}
