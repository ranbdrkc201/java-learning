package Inheritance;
class Aa
{
	Aa()
	{
		System.out.println("Class A constructor");
	}
}
class Ba extends Aa
{
	Ba()
	{
		System.out.println("Class B constructor");
	}
}
class Ca extends Ba
{
	Ca()
	{
		System.out.println("Class C constructor");
	}
}
public class ConstructorInheritance {
	public static void main(String a[])
	{
		Ca c=new Ca();
	}
}
