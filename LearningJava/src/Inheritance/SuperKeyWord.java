
//Super keWord  class
package Inheritance;
class Person
{
	String name;
	int age;
	String phone;
	Person(String name, int age, String phone)
	{
		this.name=name;
		this.age=age;
		this.phone=phone;
		
	}
}
class Student extends Person
{
	int rollNo;
	Student(String name, int age, String phone, int rollNo)
	{
		super(name, age, phone);
		this.rollNo=rollNo;
	}
	void Display()
	{
		System.out.println("Student Info...");
		System.out.println("Name Of Student="+name);
		System.out.println("Roll No Of Student="+rollNo);
		System.out.println("Age Of Student="+age);
		System.out.println("Phone No Of Student="+phone);
		
	}
}
class Teacher extends Person
{
	int salary;
	Teacher(String name, int age, String phone, int salary)
	{
		super(name, age, phone);
		this.salary=salary;
	}
	void Display()
	{
		System.out.println("...................................................");
		System.out.println("Teacher Info...");
		System.out.println("Name Of Teacher="+name);
		System.out.println("Salary Of "+name+" ="+salary);
		System.out.println("Age Of "+name+"="+age);
		System.out.println("Phone No Of "+name+"="+phone);
		
	}
}
public class SuperKeyWord {
	public static void main(String ar[])
	{
		Student s=new Student("Ran Bahadur kc", 20, "9868620708" ,12);
		Teacher t=new Teacher("Ramesh Chaudhary", 24, "9868534384" ,1200);
		s.Display();
		t.Display();
		
	}
}
