package Inheritance;
class A
{
	int x=10, y=20;
	void display()
	{
		System.out.println("This is class A");
		System.out.println("This is class B");
		System.out.println("This is class C");
		
	}
}
class B extends A
{
	void add()
	{
		System.out.println("This is class B");
		System.out.println("The sum ="+(x+y));
	}
}
class C extends A
{
	void multiplication()
	{
		System.out.println("This is class C");
		System.out.println("TheMultiplication="+(x*y));
	}
}
public class HierarchicalInheri
{
	public static void main(String a[])
	{
		B obj1= new B();
		C obj2= new C();
		obj1.display();
		obj1.add();
		obj2.multiplication();
	}
}
