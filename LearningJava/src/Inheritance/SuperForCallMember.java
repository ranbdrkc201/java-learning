package Inheritance;
class ClassA
{
	int count;
	void message()
	{
		System.out.println("Value of count super class is "+count);
	}
}
class ClassB extends ClassA
{
	int count;
	ClassB(int x, int y)
	{
		super.count=x;
		count=y;
	}
	void message()
	{
		System.out.println("Value of count in classB="+count);
	}
	void Display()
	{
		super.message();
		message();
		
	}
	
}
public class SuperForCallMember {
 public static void main(String a[])
 {
	 ClassB ob= new ClassB(200, 599);
	 ob.Display();
 }
}
