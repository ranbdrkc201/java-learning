package Inheritance;
interface Sports
{
	double sportwt=65;
	void showSportWt();
}
class Exam
{
	int roll;
	double sub1,sub2, sub3;
	public void setData(int r, double s1, double s2,double s3)
	{
		roll=r;
		sub1=s1;
		sub2=s2;
		sub3=s3;
	}
	public void showData()
	{
		System.out.println("oolNo="+roll);
		System.out.println("Subject1="+sub1);
		System.out.println("Subject2="+sub2);
		System.out.println("Subject3="+sub3);
	}
}
class Result extends Exam implements Sports
{
	double total;
	public void showSportWt()
	{
		System.out.println("Sport wt="+sportwt);
	}
	void display()
	{
		showData();
		showSportWt();
	
		total=sub1+sub2+sub3;
		System.out.println("Total="+total);
	}
	
}

public class MultipleInheritanceDemo {
	public static void main(String a[])
	{
	Result r=new Result();
	r.setData(25,87.5,90.1,95.5);
	r.display();
	}
}
