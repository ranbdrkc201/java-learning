package Swing;
import java.awt.*;
import javax.swing.*;
public class FirstClassInSwing extends JFrame{
	public FirstClassInSwing() {

		setVisible(true);
		setSize(700, 700);
		setLocation(100, 500);
		setBounds(100,50,700,500);
		ImageIcon icon=new ImageIcon("path here");
		setIconImage(icon.getImage());
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container c=getContentPane();
		c.setLayout(null);
		c.setBackground(Color.red);
	}
	
		
	public static void main(String ar[]) {
		new FirstClassInSwing();
	}
}
