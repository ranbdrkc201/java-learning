package Swing;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

/*
 * 
 * Types of Event Handiling Mechanism
 * a>Old way of event handiling
 * b> Event Deligation Method
 * 
 * 
 * 
 */
//Old way of event handiling

 class OldWayEvent extends JFrame{
	TextField tf;
	Button b;
	OldWayEvent(){
		setTitle("Old way of event Handiling");
		setSize(500,600);
		setLayout((LayoutManager) new FlowLayout());
		tf=new TextField(20);
		b=new Button("click");
		add(tf);
		add(b);
		setVisible(true);
	}
		public boolean action(Event e, Object obj) {
			String caption=(String) obj;
			if(e.target instanceof Button) {
				if(caption=="click") {
					tf.setText("Hello");
				}
			}			
		return true;
				
	}
	
}
 /* b> Event Deligation Model
  * 
  * 
  * i>Event:An event is an object that describes the state change in the source
  * ii>Event Source: A source is an object that generate an event
  * iii>Event Listener :A listener is an object that is notified when an event occurs
  * 
  * Key Event Classes
  * 1> Button, list of item is double clicked or menu item selected
  * 2>AdjustmentEvent :scrollbar
  * 3>ComponetEvent-Component moved,resizes,etc
  * 4>ContainerEvent-Componant is added or removed from the container
  * 5>FocusEvent: Component gains or losses keyboard focus
  * 6>itemEvent:Check box,or list item is clicked
  * 7>keyEvent:input received from keyboard
  * 8>MouseEvent
  * 9>TextEvent -Textarea
  * 10>WindowEvent
  * 
  * Source Of Event:
  * i>Button-Action event
  * ii>checkbox -item event
  * iii>choice---''
  * iv>List-Action event
  * v>Menu item ""
  * vi>TextComponent-textevent
  * vii>Window-window event
  * 
  * Event Listener Interface
  * i>ActionListener
  * ii>AdjustmentLis..
  * iii>ComponentLis..
  * iv>ContainerLis...
  * v>focusLis...
  * vi>ItemLis...
  * viiKeyLis..
  * viii>MousLis..
  * ix>MouseMotionLis...
  * x>MouseWheelLis..
  * xi>TextLis..
  * xii>WindowFoucusLis..
  * xiii>indowListener
  * 
  * 
  * Where to put java event Handling code?
  * 1>Same class
  * 2>outer class
  * 3>Inner Class
  * 
  * 
  */
 
 //Example of Event Handling code within the same class
 class SameClassEvent extends JFrame implements ActionListener{
	 JButton b;
	 JTextField tf;
	
	 SameClassEvent(){
		 tf=new JTextField(20);
		 tf.setBounds(60,50,170,20);
		 b=new JButton("Click me");
		 b.setBounds(80,120,100,30);
		 b.addActionListener(this);
		 add(tf);
		 add(b);
		 setSize(500,600);
		 setLayout(null);
		 setVisible(true);
	 }

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		tf.setText("Welcome");
	}
	 
 }
 
 class Sum extends JFrame implements ActionListener{
	 JButton b;
	 JTextField first,second;
	 JLabel text,set;
	
	 Sum(){
		 first=new JTextField(20);
		 second=new JTextField(20);
		 text=new JLabel("Sum =");
		 set=new JLabel();
		 
		 first.setBounds(100,50,170,20);
		 text.setBounds(300,60,170,20);
		 
		 second.setBounds(100,90,170,20);
		 set.setBounds(350,60,170,20);
		 b=new JButton("Add");
		 b.setBounds(100,120,100,30);
		 b.addActionListener(this);
		 add(first);
		 add(second);
		 add(text);
		 add(set);
		 add(b);
		 setSize(500,600);
		 setLayout(null);
		 setVisible(true);
	 }

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		int num1=Integer.parseInt(first.getText());
		int num2=Integer.parseInt(second.getText());
		int sum=(num1+num2);
		
		set.setText(""+sum);
		
	}
 }
 
 class EventDeligation{
	 
 }

public class EventHandiling {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//new OldWayEvent();
		//new SameClassEvent();
		new Sum();
		//new EventDeligation();
	}

}
