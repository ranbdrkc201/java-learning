package Swing;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
public class BorderDemo extends JFrame {
	
	BorderDemo(){
		setSize(800,800);
		JPanel content=(JPanel) getContentPane();
		content.setLayout(new GridLayout(6,2,3,3));
		JPanel p= new JPanel();
		p.setBorder(new BevelBorder(BevelBorder.RAISED));
		p.add(new JLabel("Raised Bevel Border 1"));
		content.add(p);
		
		JPanel p1= new JPanel();
		p1.setBorder(new BevelBorder(BevelBorder.LOWERED));
		p1.add(new JLabel("Raised Bevel Border 2"));
		content.add(p1);
		
		JPanel p3= new JPanel();
		p3.setBorder(new LineBorder(Color.BLACK,5));
		p3.add(new JLabel("Raised Bevel Border 2"));
		content.add(p3);
		
		JPanel p4= new JPanel();
		p4.setBorder(new TitledBorder( "Hello RN "));
		p4.add(new JLabel("Raised Bevel Border 2"));
		content.add(p4);
		
		
		setVisible(true);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new BorderDemo();
	}

}
