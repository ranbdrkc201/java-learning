package Swing;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

class RAN3d  extends JPanel{
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.setBackground(Color.WHITE);
		g.setColor(Color.BLUE);
		g.drawLine(10, 25, 200, 45);
		g.setColor(Color.RED);
		g.drawRect( 10, 55, 100, 30);
		
		g.setColor(Color.green);
		g.fillOval(10, 95, 100, 30);
		
		g.setColor(Color.orange);
		g.fill3DRect(10, 160, 100, 50, true);
	}
}

public class ThreeDRectangle {
	
	public static void main(String args[]) {
		JFrame f= new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		RAN3d p= new RAN3d();
		p.setBackground(Color.WHITE);
		f.add(p);
		f.setSize(500,700);
		f.setVisible(true);
	}

}
