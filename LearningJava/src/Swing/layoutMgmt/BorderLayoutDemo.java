/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Swing.layoutMgmt;

import java.awt.*;

/**
 *
 * @author RN
 */
public class BorderLayoutDemo extends Frame{
    
    Button b1,b2,b3,b4,b5;
    BorderLayoutDemo(){
        setSize(500,250);
        setTitle("Border Layout");
        
        setLayout(new BorderLayout());
        
        b1=new Button("NORTH");
        b2=new Button("SOUTH");
        b3=new Button("EAST");
        b4=new Button("WEST");
        b5=new Button("CENTER");
        
        add(b1,BorderLayout.NORTH);
        add(b2,BorderLayout.SOUTH);
        add(b3,BorderLayout.EAST);
        add(b4,BorderLayout.WEST);
        add(b5,BorderLayout.CENTER);
        
        
        
        setVisible(true);
    }
    
    
    public static void main(String args[]){
        new BorderLayoutDemo();
    }
    
}
