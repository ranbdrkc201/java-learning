/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Swing.layoutMgmt;

import java.awt.*;
public class CardLayoutDemo extends Frame{
    Panel p1,p2,p3;
    static CardLayout cl;
    
    CardLayoutDemo(){
        setSize(300,200);
        setTitle("Card Layout Demo");
        cl=new CardLayout();
        setLayout(cl);
        p1=new Panel();
        p2=new Panel();
        p3=new Panel();
        
        p1.setBackground(Color.red);
        p2.setBackground(Color.BLACK);
        p3.setBackground(Color.BLUE);
        p1.setForeground(Color.WHITE);
        p2.setForeground(Color.WHITE);
        p3.setForeground(Color.WHITE);
        
        add("Red Panel",p1);
        add("BLACK Panel",p2);
        add("BLUE Panel",p3);
        
        setVisible(true);
        
        
        
    }
    
    
    
    public static void main(String args[]){
        
        CardLayoutDemo first=new CardLayoutDemo();
        CardLayoutDemo second=new CardLayoutDemo();
        cl.next(second);
        CardLayoutDemo third=new CardLayoutDemo();
        cl.next(third);
    }
}
