/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Swing.layoutMgmt;

import java.awt.*;


public class GridLayoutDemo extends Frame {
    
    Button b1,b2,b3,b4,b5;
    GridLayoutDemo(){
         setSize(500,250);
        setTitle("Grid  Layout");
        
        setLayout(new GridLayout(4,3));
        
        b1=new Button("BUTTON1");
        b2=new Button("BUTTON2");
        b3=new Button("BUTTON3");
        b4=new Button("BUTTON4");
        b5=new Button("BUTTON5");
        
        add(b1);
        add(b2);
        add(b3);
        add(b4);
        add(b5);
        
        setVisible(true);
    }
    
    public static void main(String args[]){
        new GridLayoutDemo();
    }
}
