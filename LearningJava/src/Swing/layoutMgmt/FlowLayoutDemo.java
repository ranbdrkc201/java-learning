/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Swing.layoutMgmt;

import java.awt.*;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

public class FlowLayoutDemo extends Frame {
    
    Button b1,b2,b3,b4,b5,b6;
    
    FlowLayoutDemo(){
        setSize(400,150);
        setTitle("Flow Layout Management");
        setLayout(new FlowLayout());
        b1=new Button("Button 1");
        b2=new Button("Button 2");
        b3=new Button("Button 3");
        b4=new Button("Button 4");
        b5=new Button("Button 5");
        b6=new Button("Button 6");
        add(b1);
        add(b2);
        add(b3);
        add(b4);
        add(b5);
        add(b6);
        
        
        
        
        
        
        setVisible(true);
    }
    
    public static void main(String args[]){
        new FlowLayoutDemo();
    }
}
