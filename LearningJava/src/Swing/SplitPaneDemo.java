package Swing;
import javax.swing.*;
import java.awt.*;

public class SplitPaneDemo extends JFrame {
	JSplitPane sp;
	SplitPaneDemo(){
		JFrame frame= new JFrame();
		setSize(600,600);
		
		JPanel p1= new JPanel();
		JPanel p2=new JPanel();
		
		
		p1.setPreferredSize(new Dimension(300,100));
		sp= new JSplitPane(JSplitPane.VERTICAL_SPLIT,p1,p2);
		add(sp,BorderLayout.CENTER);
		setVisible(true);
	}

	public static void main(String[] args) {
		new SplitPaneDemo();
	}

}
