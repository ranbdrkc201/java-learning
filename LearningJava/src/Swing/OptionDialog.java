package Swing;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/*
 * Syntax:
 * object option[]={"yes Please", "No way"}
 * JOptainPane.showOptionDialog(parent_frame, msg, title,type_type,msg_type,icon,option,default_option)
 * 
 * 
 */

public class OptionDialog {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JFrame frame= new JFrame();
		frame.setLocationRelativeTo(null);
		frame.setSize(500,500);
		ImageIcon icon= new ImageIcon("E:\\Icon\\alert-triangle-red-512.png");
		Object option[]= {"Yes Please","No way"};
		JOptionPane.showOptionDialog(frame, "Do you like green color", "choice", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE,icon, option, option[0]);
		

	}

}
