package Swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.*;

public class InnerClassEventDemo extends JFrame {
	JTextField tf;
	JButton b;
	InnerClassEventDemo()
	 {
		 tf=new JTextField();
		 tf.setBounds(60,50,170,20);
		 b=new JButton("Click me");
		 b.setBounds(80,120,100,30);
		 
		 b.addActionListener(new Handler());
		 add(tf);
		 add(b);
		 setSize(700,500);
		 setLocationRelativeTo(null);
		 setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		 //setLayout(null);
		 setVisible(true);
		 
	 }

	public class Handler implements ActionListener{
		// TODO Auto-generated method stub
		public void actionPerformed(ActionEvent e){  
			tf.setText("welcome Rn");  
			}  
	}
	public static void main(String args[]) {
		new InnerClassEventDemo();
	}
}

