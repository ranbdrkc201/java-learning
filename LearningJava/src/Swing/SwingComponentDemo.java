package Swing;

import java.awt.Color;
import java.awt.Container;

import javax.swing.*;
public class SwingComponentDemo extends JFrame {
	
	public SwingComponentDemo() {
		// TODO Auto-generated constructor stub
		setSize(600,700);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("MY Title");
		setLayout(null);
		
		
	JLabel l=new JLabel("User Name:");
	JLabel pass=new JLabel("Password:");
	JLabel gender=new JLabel("Gender:");
	JLabel comment=new JLabel("Comment:");
	JTextArea jarea= new JTextArea();
	JRadioButton radio1=new JRadioButton("Male");
	JRadioButton radio2=new JRadioButton("Female");
	JButton save= new JButton("Save");
	JButton cancel= new JButton("Cancel");
	
		JTextField t= new JTextField(50);
		JPasswordField p= new JPasswordField("");
		l.setBounds(150,100,100,30);
		gender.setBounds(150,200,100,30);
		comment.setBounds(150,250,100,30);
		pass.setBounds(150,150,100,30);
		add(l);
		add(pass);
		add(gender);
		add(comment);
		t.setBounds(250,100,100,30);
		add(t);
		p.setBounds(250,150,100,30);
		add(p);
		radio1.setBounds(250,200,100,30);
		add(radio1);
		radio2.setBounds(350,200,100,30);
		add(radio2);
		jarea.setBounds(250,250,100,30);
		add(jarea);
		save.setBounds(250,800,100,30);
		
		jarea.setSize(300, 100);
		add(save);
		cancel.setBounds(380,800,100,30);
		add(cancel);
		
		setVisible(true);
	}

		
		
	public static void main(String ar[]) {
		new SwingComponentDemo();
	}
}

