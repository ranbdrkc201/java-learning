package Swing;

import java.awt.FlowLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class FocusEventDemo extends JFrame implements FocusListener {
	JLabel l1,l2,l3;
	JTextField t1,t2,t3;
	JButton b,b1,b2,b3;
	 public FocusEventDemo() {
		// TODO Auto-generated constructor stub
		 l1=new JLabel("First Number");
		 l1.setBounds(200,50,50,100);
		 
		 t1.setBounds(200,50,50,100);
		 t2=new JTextField(20);
		 
		 l2=new JLabel("Second Number");
		 l2.setBounds(200,100,50,100);
		 
		 l3=new JLabel("Result");
		 l3.setBounds(200,150,50,100);
		 t1=new JTextField(20);
		 
		 t1.setBounds(200,50,50,100);
		 t2=new JTextField(20);
		 
		 t2.setBounds(200,50,50,100);
		 t3=new JTextField(20);
		 
		 t3.setBounds(100,150,50,100);
		 b=new JButton("Add");
                 b.setBounds(100,400,50,100);
                  b1=new JButton("Sub");
                 b1.setBounds(200,150,50,100);
                  b2=new JButton("Mul");
                 b2.setBounds(250,150,50,100);
                  b3=new JButton("Div");
                 b3.setBounds(300,150,50,100);
                 b.addFocusListener(this);
                 t1.addFocusListener(this);
                 t2.addFocusListener(this);
                 
			 add(l1);
			 add(t1);
			 add(l2);
			 add(t2);
			 add(l3);
			 add(t3);
			 add(b);
                 add(b1);
                 add(b2);
                 add(b3);
		 setSize(700,800);
		 setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		 setLayout(new FlowLayout(FlowLayout.CENTER,200,10));
		 setVisible(true);
	}
	
    
	


	@Override
	public void focusGained(FocusEvent e) {
		 int x,y,z=0;
         x=Integer.parseInt(t1.getText());
        y=Integer.parseInt(t2.getText());
        if(e.getSource()==b){
            x=Integer.parseInt(t1.getText());
            y=Integer.parseInt(t2.getText());
            z=x+y;
            t3.setText(String.valueOf(z));
        }else{
            t3.setText("Press a to add");
        }
		
	}


	@Override
	public void focusLost(FocusEvent e) {
		if(e.getSource()==t1 && t1.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "Please Enter a to continue");
			t1.requestFocus();
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new FocusEventDemo();
	}

}
