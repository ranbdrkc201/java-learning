package Swing;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;

import javax.swing.*;

public class TextEventDemo extends JFrame implements TextListener {
JLabel l;

	JTextField tf;
	TextEventDemo(){
		l=new JLabel("Hello");
		tf=new JTextField(20);
		add(l);
		add(tf);
		tf.addTextListener();
		setSize(500,500);
		setLayout(null);
		setVisible(true);
		
	}

	@Override
	public void textValueChanged(TextEvent e) {
		// TODO Auto-generated method stub
		l.setText("You Typed"+tf.getText());
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new TextEventDemo();

	}

}
