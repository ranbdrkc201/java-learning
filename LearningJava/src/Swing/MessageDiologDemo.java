package Swing;

import javax.swing.*;
import java.awt.*;

public class MessageDiologDemo {

	public static void main(String[] args) {
		
		// JOationPane.showMessageDialog(parent_frame,msg, title,msg_type,icon)
		/*  Dialog Boxs
		 * 1.Message Dialog
		 * 2.Confirm Dialog
		 * 3.Option Dialog
		 * 4.Input Dialog
		 * 
		 *  Msg Type
		 * 1. INFORMATION MESSAGE
		 * 2.WARNING MESSAGE
		 * 3.QUESTION MESSAGE
		 * 4.PLAIN
		 * 
		 * Confirm Message
		 * type
		 * 1.DEFAULT_OPTION
		 * 2.YES_NO_CANCEL_OPTION
		 * 3.YES_NO_OPTION
		 * 4.OK_CANCEL_OPTION
		 */
		JFrame frame= new JFrame();
			frame.setLocationRelativeTo(null);
			frame.setSize(500,500);
			ImageIcon icon= new ImageIcon("E:\\Icon\\alert-triangle-red-512.png");
			JOptionPane.showMessageDialog(frame, "Wrong");
			JOptionPane.showConfirmDialog(frame, "Wrong", "Worng", JOptionPane.DEFAULT_OPTION);
			JOptionPane.showConfirmDialog(frame, "Wrong", "Worng", JOptionPane.YES_NO_CANCEL_OPTION);
			JOptionPane.showConfirmDialog(frame, "Wrong", "Worng", JOptionPane.YES_NO_OPTION);
			JOptionPane.showConfirmDialog(frame, "Wrong", "Worng", JOptionPane.YES_OPTION);
			JOptionPane.showConfirmDialog(frame, "Wrong", "Worng", JOptionPane.OK_CANCEL_OPTION);
			JOptionPane.showConfirmDialog(frame, "Wrong", "Worng", JOptionPane.OK_OPTION);
			JOptionPane.showConfirmDialog(frame, "Wrong");

	}

}
