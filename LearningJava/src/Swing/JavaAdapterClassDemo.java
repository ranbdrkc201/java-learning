package Swing;
/* Adapter Classes                       Listener Interface
 * 1.ComponentAdapter					ComponentListener
 * 2.ContainerAapter					containerListener
 * 3.FocuseAda.
 * 4.KeyAdap
 * 5.MouseAda.
 * 6.MouseMotionAda
 * 7.windowAda							sameBut Listener
 */


import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;

import javax.swing.*;

import com.sun.glass.events.MouseEvent;


public class JavaAdapterClassDemo extends MouseAdapter {
	JFrame frame;
	JavaAdapterClassDemo(){
		frame=new JFrame("Mouse Adapter");
		frame.addMouseListener(this);
		frame.setSize(600, 700);
		frame.getDefaultCloseOperation();
		frame.setLayout(null);
		frame.setVisible(true);
		
	}
	public void mouseClicked(MouseEvent e) {
		Graphics g=frame.getGraphics();
		g.setColor(Color.BLUE);
		//g.fillOval(e.getX(), e.getY(), 30, 30);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new JavaAdapterClassDemo();

	}

}
