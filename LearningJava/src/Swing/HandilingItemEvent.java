package Swing;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.*;

public class HandilingItemEvent extends JFrame implements ItemListener {
	
	JLabel l;
	JRadioButton r1,r2;
	public HandilingItemEvent() {
		// TODO Auto-generated constructor stub
		l=new JLabel("Gender");
		l.setBounds(50,50,50,50);
		r1=new JRadioButton("Male");
		r1.setBounds(50,150,0,50);
		r2=new JRadioButton("Female");
		r2.setBounds(150,150,50,50);
		r1.addItemListener(this);
		r2.addItemListener(this);
		add(l);
		add(r1);
		add(r2);
		setSize(500,600);
		setLayout(null);
		setVisible(true);
		
		
	}
	
	
	
	@Override
	public void itemStateChanged(ItemEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==r1 && r1.isSelected()==true) {
			JOptionPane.showMessageDialog(this, "You are male");
		}
		else if(e.getSource()==r2 && r2.isSelected()==true) {
			JOptionPane.showMessageDialog(this, "You are female");
		}else {
			//JOptionPane.showMessageDialog(this, "Sorry");
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new HandilingItemEvent();
	}

}
