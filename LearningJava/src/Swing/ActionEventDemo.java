package Swing;

import java.awt.FlowLayout;
import java.awt.event.*;

import javax.swing.*;

public class ActionEventDemo extends JFrame implements ActionListener {
	JLabel l1,l2,l3;
	JTextField t1,t2,t3;
	JButton b;
	 public ActionEventDemo() {
		// TODO Auto-generated constructor stub
		 l1=new JLabel("First Number");
		 l1.setBounds(150,50,50,100);
		 l2=new JLabel("Second Number");
		 l2.setBounds(150,100,50,100);
		 l3=new JLabel("Result");
		 t1=new JTextField(10);
		 t1.setBounds(100,50,50,100);
		 t2=new JTextField(10);
		 t2.setBounds(100,100,50,100);
		 t3=new JTextField(10);
		 t3.setBounds(100,150,50,100);
		 b=new JButton("Add");
                 b.setBounds(100,400,50,100);
                
		 b.addActionListener(this);
               
		 add(l1);
		 add(t1);
		 add(l2);
		 add(t2);
		 add(l3);
		 add(t3);
		 add(b);
               
		 setSize(700,800);
		 setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		 setLayout(new FlowLayout(FlowLayout.CENTER,200,10));
		 setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		int x,y,z;
                x=Integer.parseInt(t1.getText());
		y=Integer.parseInt(t2.getText());
                if(e.getActionCommand().equals("Add")){
                    z=x+y;
                    t3.setText(String.valueOf(z));
                }
               
	}
	public static void main(String args[]) {
		new ActionEventDemo();
	}

}
