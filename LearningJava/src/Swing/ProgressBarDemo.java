package Swing;
import javax.swing.*;

public class ProgressBarDemo extends JFrame {
	 JProgressBar jb;
	 int i=0;
	 ProgressBarDemo(){
		 jb=new JProgressBar(0,2000);
		 jb.setValue(0);
		 jb.setBounds(40,40,160,30);
		 jb.setStringPainted(true);
		 add(jb);
		 setSize(700,600);
		 setLayout(null);
	 }
	 public void iterate() {
		 while(i<=2000) {
			 jb.setValue(i);
			 i=i+20;
		 
		 try {
			 Thread.sleep(10);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		 }
	 }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ProgressBarDemo p=new ProgressBarDemo();
		p.setVisible(true);
		p.iterate();

	}

}
