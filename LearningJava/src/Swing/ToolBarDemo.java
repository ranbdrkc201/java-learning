package Swing;

import java.awt.BorderLayout;

import javax.swing.*;

public class ToolBarDemo {

	public static void main(String[] args) {
		
		JFrame frame= new JFrame();
		frame.setLocationRelativeTo(null);
		frame.setSize(500,500);
		JToolBar toolbar=new JToolBar();
		JButton b1= new JButton("Button 1");
		JButton b2= new JButton("Button 2");
		b1.setToolTipText("This is button 1");
		b2.setToolTipText("This is button 2");
		toolbar.add(b1);
		toolbar.add(b2);
		frame.add(toolbar, BorderLayout.NORTH);
		frame.setVisible(true);
	}

}
