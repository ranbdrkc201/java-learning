package Swing;

import javax.swing.*;
import java.awt.*;

public class TabbedPaneDemo extends JFrame {
	JTabbedPane tp;
	TabbedPaneDemo() {
		setSize(600,600);
		setTitle("Default");
		JPanel p1=new JPanel();
		JPanel p2=new JPanel();
		
		p1.setBackground(getBackground());

		JLabel lable1=new JLabel("This is  1st Tab");
		JLabel lable2=new JLabel("This is  2st Tab");
		
		tp= new JTabbedPane(JTabbedPane.TOP);
		tp.addTab("First Tab", p1);
		tp.addTab("Second Tab", p2);
		add(tp);
		setVisible(true);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new TabbedPaneDemo();
	}

}
