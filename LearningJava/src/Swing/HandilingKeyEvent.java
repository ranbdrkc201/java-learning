
package Swing;

import java.awt.HeadlessException;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.*;

/*
    Handiling Key Event :
    1.KeyPressed()
    2.KeyReleased()
    3.KeyTyped();


*/
public class HandilingKeyEvent extends JFrame implements KeyListener {

    JLabel l1,l2,l3;
    JTextField t1,t2,t3;
    JButton b;

    public HandilingKeyEvent(){
                l1=new JLabel("First Number");
		 l1.setBounds(150,50,50,100);
		 l2=new JLabel("Second Number");
		 l2.setBounds(150,100,50,100);
		 l3=new JLabel("Result");
		 t1=new JTextField(10);
		 t1.setBounds(100,50,50,100);
		 t2=new JTextField(10);
		 t2.setBounds(100,100,50,100);
		 t3=new JTextField(10);
		 t3.setBounds(100,150,50,100);
		 b=new JButton("Add");
                 b.setBounds(100,100,50,100);
//                  b1=new JButton("Sub");
//                 b1.setBounds(200,150,50,100);
//                  b2=new JButton("Mul");
//                 b2.setBounds(250,150,50,100);
//                  b3=new JButton("Div");
//                 b3.setBounds(300,150,50,100);
		 b.addKeyListener(this);
//                 b1.addActionListener(this);
//                 b2.addActionListener(this);
//                 b3.addActionListener(this);
		 add(l1);
		 add(t1);
		 add(l2);
		 add(t2);
		 add(l3);
		 add(t3);
		 add(b);
//                 add(b1);
//                 add(b2);
//                 add(b3);
		 setSize(700,800);
		 setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		 setLayout(new FlowLayout(FlowLayout.CENTER,200,10));
		 setVisible(true);
    }
    
    
    
    
    
    
    
    
    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int x,y,z=0;
         x=Integer.parseInt(t1.getText());
        y=Integer.parseInt(t2.getText());
        if(e.getKeyChar()=='a'){
            z=x+y;
        }else{
            t3.setText("Press a to add");
        }
        t3.setText(String.valueOf(z));
    }

    @Override
    public void keyReleased(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public static void main(String args[]){
        new HandilingKeyEvent();
    }
    
}
