package Swing;

import java.awt.event.WindowAdapter;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.sun.glass.events.WindowEvent;

public class WindowEventDemo extends WindowAdapter {
	JFrame f;
	public WindowEventDemo() {
		f= new JFrame("Window EventDemo");
		f.setSize(600, 400);
		f.addWindowListener(this);
		f.setVisible(true);
		
	}
	public void windowOpened(WindowEvent e) {
		JOptionPane.showMessageDialog(f,"Welcome");
	}
	public void windowClosing(WindowEvent e) {
		JOptionPane.showMessageDialog(f,"Good Bye");
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new WindowEventDemo();
	}

}
