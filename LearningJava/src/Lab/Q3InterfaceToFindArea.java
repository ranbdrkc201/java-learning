package Lab;
interface InterfaceClass{
    public void areaOfCircle(float r);
    public void areaOfRectangle(float l,float b);
}
class caculateArea implements InterfaceClass{
    public void areaOfCircle(float r){
        float result= (float) 3.14*r*r;
        System.out.println("Area Of Circle="+result);
    }
    public void areaOfRectangle(float l, float b) {
        float result=l*b;
        System.out.println("Area Of Rectangle="+result);
    }
}
public class Q3InterfaceToFindArea {
    public static void main(String args[]){
      caculateArea ob=new caculateArea();
      ob.areaOfCircle(5);
      ob.areaOfRectangle(4, 5);
    }
}
