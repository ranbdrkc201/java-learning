package Lab;
public class Q5Overloading {
    public void Test(){
        System.out.println("Overloading Method");
    }
    public void Test(String name){
        System.out.println("Name="+name);
    }
    public void Test(int rollNo){
        System.out.println("Roll No="+rollNo);
    }  
    public static void main(String args[]){
        Q5Overloading ob=new Q5Overloading();
        ob.Test();
        ob.Test("Khatri");
        ob.Test(8787);
    }
}
