
package Lab;

abstract class FindArea{
    abstract void areaOfTriangle(float b, float h);
    abstract void areaOfRectangle(float b, float h);
}

class CalculateArea extends FindArea{

    void areaOfTriangle(float b, float h) {
        float result=(b*h)/2;
        System.out.println("Area Of Traingle ="+result);
    }

    void areaOfRectangle(float l, float b) {
         float result=l*b;
        System.out.println("Area Of Rectangle ="+result);
    }
    
}

public class Q1AbstractClassFindArea {
    public static void main(String args[]){
        FindArea fa=new CalculateArea();
        fa.areaOfTriangle(20,10);
        fa.areaOfRectangle(5,4);
    }
}
