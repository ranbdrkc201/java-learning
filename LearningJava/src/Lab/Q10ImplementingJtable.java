
package Lab;
import javax.swing.*;
public class Q10ImplementingJtable{
    JFrame f; 
    Q10ImplementingJtable(){
         f=new JFrame();    
        String data[][]={ {"101","RN","670000"},    
                          {"102","HARI","780000"},    
                          {"101","GOBINDA","700000"}};    
        String column[]={"ID","NAME","SALARY"};         
        JTable jt=new JTable(data,column);    
        jt.setBounds(30,40,200,300);          
        JScrollPane sp=new JScrollPane(jt);    
        f.add(sp); 
        f.setSize(300,400);
        f.setVisible(true);
    }
    public static void main(String args[]){
        new Q10ImplementingJtable();
    }
}
