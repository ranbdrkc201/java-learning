package Lab;
import java.util.Scanner;

public class Q8ThrowException {
   public static void main(String args[]){
       String name;
       int age;
       Scanner sc=new Scanner(System.in);
       System.out.println("Enter the Name");
       name=sc.next();
       System.out.println("Enter the Age");
       age=sc.nextInt();
       try{
            if(name.equals("Ram")){
                System.out.println("You are Ram");
            } else{
                 throw new Exception("there is an error");
            }
       }
       catch(Exception e){
           System.out.println(e.getMessage());
       }
   }
}
