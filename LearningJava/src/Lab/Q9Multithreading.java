
package Lab;
class Aa extends Thread{
    public void run()
    {
	for(int i=0; i<5; i++) {
            System.out.println("From thread A:i=" +i);
	}
	System.out.println("Exit From A");
    }
}
class Bb extends Thread{
    public void run() {
	for(int j=0; j<5; j++) {
            System.out.println("From thread B: j="+j);
	}
	System.out.println("Exit From B");
    }
}
public class Q9Multithreading {
    public static void main(String args[]) {
	Aa a=new Aa();
	Bb b= new Bb();
	a.start();
	b.start();
    }
}
