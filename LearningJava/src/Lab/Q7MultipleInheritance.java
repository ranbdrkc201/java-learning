package Lab;
class A{
    String name;
    
    void display(){
        System.out.println("Class A");
    }
}
class B extends A{
    public B(String name){
        this.name=name;
    }
    public void dispaly(){
         System.out.println("Class B ="+name);
    }
}
class C extends A{
    
    public C(String name) {
        this.name=name;
    }
    public void dispaly(){
         System.out.println("Class C ="+name);
    }
    
}
public class Q7MultipleInheritance {
    public static void main(String args[]){
        A ob=new A();
        ob.display();
        B b=new B("ABC");
        b.dispaly();
        C c=new C("XYZ");
        c.dispaly();
        
    }
}
