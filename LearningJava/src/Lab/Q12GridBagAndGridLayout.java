/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab;

import java.awt.*;
import javax.swing.*;
public class Q12GridBagAndGridLayout extends JFrame{
    Button b1,b2,b3,b4,b5; 
    public void GridBagLayout(){
         setSize(500,250);
        setTitle("Grid  Layout");        
        setLayout(new GridLayout(4,3));        
        b1=new Button("BUTTON1");
        b2=new Button("BUTTON2");
        b3=new Button("BUTTON3");
        b4=new Button("BUTTON4");
        b5=new Button("BUTTON5");       
        add(b1);
        add(b2);
        add(b3);
        add(b4);
        add(b5);    
        setVisible(true);
    }   
    public void GridLayout(){
            GridBagLayout grid = new GridBagLayout();
            GridBagConstraints gbc = new GridBagConstraints();  
            setLayout(grid);  
            setTitle("GridBag Layout Example");  
            GridBagLayout layout = new GridBagLayout();  
            this.setLayout(layout);  
            gbc.fill = GridBagConstraints.HORIZONTAL;  
            gbc.gridx = 0;  
            gbc.gridy = 0;  
            this.add(new Button("Button One"), gbc);  
            gbc.gridx = 1;  
            gbc.gridy = 0;  
            this.add(new Button("Button two"), gbc);  
            gbc.fill = GridBagConstraints.HORIZONTAL;  
            gbc.ipady = 20;  
            gbc.gridx = 0;  
            gbc.gridy = 1;  
            this.add(new Button("Button Three"), gbc);  
            gbc.gridx = 1;  
            gbc.gridy = 1;  
            this.add(new Button("Button Four"), gbc);  
            gbc.gridx = 0;  
            gbc.gridy = 2;  
            gbc.fill = GridBagConstraints.HORIZONTAL;  
            gbc.gridwidth = 2;  
            this.add(new Button("Button Five"), gbc);  
            setSize(300, 300);  
            setPreferredSize(getSize());  
            setVisible(true);  
            setDefaultCloseOperation(EXIT_ON_CLOSE);  
    }  
    public static void main(String args[]){
       Q12GridBagAndGridLayout ob= new Q12GridBagAndGridLayout();
       ob.GridBagLayout();
       //ob.GridLayout();
    }
}
