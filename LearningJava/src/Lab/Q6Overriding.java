
package Lab;
class FirstClass{
   int x;
   FirstClass(int x){
       this.x=x;
   }
   void display(){
       System.out.println("First Class");
   }
}
class SecondClass extends FirstClass{
    int y;
     SecondClass(int x,int y){
         super(x);
         this.y=y;
     }
     void display(){
         super.display();
         System.out.println("First Class value="+x);
         System.out.println("Second Class value="+y);
     }  
}
public class Q6Overriding {
    public static void main(String args[]){
        SecondClass ob=new SecondClass(200,300);
        ob.display();
    }
}
