class RetTest
{
	int a;
	RetTest(int i)
	{
		a=i;
	}
	RetTest incrementByTest()
	{
		RetTest temp=new RetTest(a+10);
		return temp;
	}
}
public class RetObject {
	public static void main(String a[]) {
		RetTest obj= new RetTest(100);
		System.out.println("Obj1="+obj.a);
		RetTest obj2= obj.incrementByTest();
		System.out.println("Obj2="+obj2.a);
	}
}
