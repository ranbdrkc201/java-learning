
package DbConnectivity;

import java.sql.*;
public class JDBC2072Q3Ans {
    Connection con;
    Statement stmt;
    ResultSet rs;
    JDBC2072Q3Ans(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc","root","1234");
        }catch(Exception e){
            System.out.println(e);
        }
    }
    public void fetchRecord(){
        String sql="SELECT * from student where district='Kathmandu'";
        try{
            stmt=con.createStatement();            //ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE under the while statement write  rs.updateString("age", "33");
       //         rs.updateRow(); update the row
            rs=stmt.executeQuery(sql);
//            ResultSetMetaData metaData=rs.getMetaData();
//            int noOfCol=metaData.getColumnCount();
//            for(int i=1;i<=noOfCol;i++){
//                System.out.printf("%-8s \t",metaData.getColumnName(i));
//            }
//            System.out.println();
            while(rs.next()){
                System.out.println("Name :" +rs.getString("name"));
               
            }
        }
        catch(Exception e){
            System.out.println(e);
        }
//        finally{
//           after the close the result set and statement we use try Catch block used
//        }
    }
    
    public static void main(String args[]){
        JDBC2072Q3Ans ob=new JDBC2072Q3Ans();
        ob.fetchRecord();
    }
}
