class Base
{
	int a;
	Base(  int x)
	{
		a=x;
	}
	void Display()
	{
		System.out.println("Super a="+a);
	}
}
class Derived extends Base
{
	int b;
	public Derived(int x, int y) {
		// TODO Auto-generated constructor stub
		super(x);
		b=y;
	}
	void Display()
	{
		
		System.out.println("Super in derived a="+a);
		System.out.println("Super in derived b="+b);
	}
}
public class OverridingMethods {
 public static void main(String a[]) {
	 Derived d=new Derived(100, 400);
	d.Display();
 }
}
